"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""


def read_str_from_file(str1):
    with open(str1, 'r') as x:
        for line in x:
            print(line.strip())


if __name__ == '__main__':
    read_str_from_file('my_dict.txt')
