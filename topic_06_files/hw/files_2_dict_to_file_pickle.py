"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""

import pickle


def save_dict_to_file_pickle(path1, my_dict):
    with open(path1, 'wb') as pickle_file:
        pickle.dump(my_dict, pickle_file)


if __name__ == '__main__':
    dict1 = {1: 'one', 2: 'two', 3: 'three'}
    path2 = 'dict.pkl'

    save_dict_to_file_pickle(path2, dict1)

    with open(path2, 'rb') as f:
        dict2 = pickle.load(f)

    print(f'type: {type(dict2)}')
    print(f'equal: {dict1 == dict2}')
