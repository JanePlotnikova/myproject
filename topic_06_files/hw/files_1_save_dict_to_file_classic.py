"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic(path, my_dict):
    with open(path, 'w') as file1:
        file1.write(str(my_dict))


if __name__ == '__main__':
    path1 = 'my_dict.txt'
    save_dict_to_file_classic(path1, {1: 'one', 2: 'two', 3: 'three'})

    with open(path1, 'r') as file2:
        my_dict_str = file2.read()
        print(my_dict_str)
