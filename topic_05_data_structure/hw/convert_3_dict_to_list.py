"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(my_dict: dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    if len(my_dict) == 0:
        return [], [], 0, 0

    keys_list = list(my_dict.keys())
    val_list = list(my_dict.values())
    len_unique_keys = len(set(keys_list))
    len_unique_val = len(set(val_list))

    return keys_list, val_list, len_unique_keys, len_unique_val


if __name__ == '__main__':
    print(dict_to_list({1: 0, 2: 9, 3: 8, 4: 7}))
