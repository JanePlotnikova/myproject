"""
Функция count_word.

Принимает 2 аргумента:
список слов my_list и
строку word.

Возвращает количество word в списке my_list.

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо строки передано что-то другое, то возвращать строку 'Second arg must be str!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""

def count_word(my_list: list, word: str):
    if type(my_list) != list:
        return 'First arg must be list!'
    elif type(word) != str:
        return 'Second arg must be str!'
    elif my_list == []:
        return 'Empty list!'

    return my_list.count(word)