"""
Функция pow_odd.

Принимает число n.

Возвращает список, состоящий из квадратов нечетных чисел в диапазоне от 0 до n (не включая).

Пример: n = 7, нечетные числа [1, 3, 5], результат [1, 9, 25]

Если n не является int, то вернуть строку 'Must be int!'.
"""

def pow_odd(n: int):
    if type(n) != int:
        return 'Must be int!'
    return [n ** 2 for n in range(0, n) if n % 2 != 0]


if __name__ == '__main__':
    print(pow_odd(7))