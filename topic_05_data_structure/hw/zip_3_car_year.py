"""
Функция zip_car_year.

Принимает 2 аргумента: список с машинами и список с годами производства.

Возвращает список с парами значений из каждого аргумента, если один список больше другого,
то заполнить недостающие элементы строкой "???".

Подсказка: zip_longest.

Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
"""
from itertools import zip_longest


def zip_car_year(car: list, year: list):
    if type(car) != list or type(year) != list:
        return 'Must be list!'
    if car == [] or year == []:
        return 'Empty list!'
    return list(zip_longest(car, year, fillvalue='???'))


if __name__ == '__main__':
    print(zip_car_year(['Ferrari', 'Porsche', 'Audi'], ['1984', '2018', '2020', '1970']))
