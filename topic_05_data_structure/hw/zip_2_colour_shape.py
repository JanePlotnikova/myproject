"""
Функция zip_colour_shape.

Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с формами (квадрат, круг).

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо tuple передано что-то другое, то возвращать строку 'Second arg must be tuple!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если tuple пуст, то возвращать строку 'Empty tuple!'.

Если list и tuple различного размера, обрезаем до минимального (стандартный zip).
"""

def zip_colour_shape(colour_list: list, form: tuple):
    if type(colour_list) != list:
        return 'First arg must be list!'
    if type(form) != tuple:
        return 'Second arg must be tuple!'
    if colour_list == []:
        return 'Empty list!'
    if form == ():
        return 'Empty tuple!'
    return list(zip(colour_list, form))

if __name__ == '__main__':
    print(zip_colour_shape(['red', 'green', 'blue', 'yellow'], ('square', 'triangle', 'circle')))