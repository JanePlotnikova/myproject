"""
Функция get_translation_by_word.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (ru).

Возвращает все варианты переводов (list), если такое слово есть в словаре,
если нет, то "Can't find Russian word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_translation_by_word(ru_word: dict, ru: str):
    if type(ru_word) != dict:
        return "Dictionary must be dict!"
    elif type(ru) != str:
        return "Word must be str!"
    elif ru_word == {}:
        return "Dictionary is empty!"
    elif len(ru) == 0:
        return "Word is empty!"
    else:
        return ru_word.get(ru, f"Can't find Russian word: {ru}")


if __name__ == '__main__':
    ru_word = {'хороший': ['good', 'nice'],
               'плохой': ['awful', 'bad']}
    print(get_translation_by_word(ru_word, 'плохой'))
    print(get_translation_by_word(ru_word, 'маленький'))
    print(get_translation_by_word('тест', 'плохой'))
