"""
Функция magic_mul.

Принимает 1 аргумент: список my_list.

Возвращает список, который состоит из
[первого элемента my_list]
+ [три раза повторенных списков my_list]
+ [последнего элемента my_list].

Пример:
входной список [1,  'aa', 99]
результат [1, 1,  'aa', 99, 1,  'aa', 99, 1,  'aa', 99, 99].

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""

def magic_mul(my_list: list):
    if type(my_list) != list:
        return 'Must be list!'
    elif my_list == []:
        return 'Empty list!'
    else:
        result = []
        first_el = my_list[:1]
        repeat = my_list * 3
        last_el = my_list[-1]
        result.extend(first_el)
        result.extend(repeat)
        result.append(last_el)
        return result
print(magic_mul([1,  'aa', 99]))