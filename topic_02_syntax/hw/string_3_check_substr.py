"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(str1, str2):
    if str1 == str2:
        return False
    elif str1 == "" or str2 == "":
        return True
    elif len(str1) < len(str2) and str1 in str2 or len(str2) < len(str1) and str2 in str1:
        return True
    else:
        return False


if __name__ == 'main':
    result = check_substr("Поклон", "клон")
    print(result)
