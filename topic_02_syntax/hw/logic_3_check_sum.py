"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""

def check_sum(x, y, z):
    if (x + y == z) or (x + z == y) or (y + z == x):
        return True
    else:
        return False