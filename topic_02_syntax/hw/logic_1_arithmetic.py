"""
Функция arithmetic.

Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
Если третий аргумент +, сложить их;
если —, то вычесть;
если *, то умножить;
если /, то разделить (первое на второе).
В остальных случаях вернуть строку "Unknown operator".
Вернуть результат операции.
"""

def arithmetic(x, y, sign):
    if sign == '+':
        return x + y
    elif sign == '-':
        return x - y
    elif sign == '*':
        return x * y
    elif sign == '/':
        return x / y
    else:
        return 'Unknown operator'

if __name__ == '__main__':
    x = 4
    y = 5
    sign = '+'
    my_result = arithmetic(x, y, sign)
    print(f"{x} {sign} {y} = {my_result}")
