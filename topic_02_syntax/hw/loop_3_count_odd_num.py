"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число меньше или равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""

def count_odd_num(number):
    if type(number) != int:
        return "Must be int!"
    if number <= 0:
        return "Must be > 0!"
    else:
        odd_count = 0
        number_string = str(number)
        for sign in number_string:
            if int(sign) % 2 != 0:
                odd_count += 1

    return odd_count