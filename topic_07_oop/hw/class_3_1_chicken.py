"""
Класс Chicken.

Поля:
имя: name,
номер загона: corral_num,
сколько яиц в день: eggs_per_day.

Методы:
get_sound: вернуть строку 'Ко-ко-ко',
get_info: вернуть строку вида:
"Имя курицы: name
 Номер загона: corral_num
 Количество яиц в день: eggs_per_day"
__lt__: вернуть результат сравнения количества яиц (<)
"""


class Chicken:

    def __init__(self, n, c_n, e_p_d):
        self.name = n
        self.corral_num = c_n
        self.eggs_per_day = e_p_d

    def get_sound(self):
        return "Ко-ко-ко"

    def get_info(self):
        return f'Имя курицы: {self.name}\nНомер загона: {self.corral_num}\nКоличество яиц в день: {self.eggs_per_day}'

    def __lt__(self, other):
        return self.eggs_per_day < other.eggs_per_day


if __name__ == '__main__':
    chicken1 = Chicken("Koko", 2, 15)
    chicken2 = Chicken('Kukareku', 3, 17)
    print(chicken1.get_info())
    print(chicken2.get_info())
    if chicken1 < chicken2:
        print('Chicken1 brings fewer eggs')
    else:
        print('Chicken1 brings more eggs')
    print(chicken2.get_sound())
