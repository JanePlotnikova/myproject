"""
Класс Goat.

Поля:
имя: name,
возраст: age,
сколько молока дает в день: milk_per_day.

Методы:
get_sound: вернуть строку 'Бе-бе-бе',
__invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
__mul__: умножить milk_per_day на число. вернуть self
"""


class Goat:
    def __init__(self, n: str, a: int, m_d: int):
        self.name = n
        self.age = a
        self.milk_per_day = m_d

    def get_sound(self):
        return 'Бе-бе-бе'

    def __invert__(self):
        self.name = self.name[::-1].title()
        return self

    def __mul__(self, other):
        self.milk_per_day = self.milk_per_day * other
        return self


if __name__ == '__main__':
    g1 = Goat('Goat1', 5, 2)
    g2 = Goat('Goat2', 4, 3)
    print(g1.get_sound())
    print(g2)
