"""
Класс Farm.

Поля:
животные (list из произвольного количества Goat и Chicken): zoo_animals
(вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
наименование фермы: name,
имя владельца фермы: owner.

Методы:
get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
get_chicken_count: вернуть количество куриц на ферме,
get_animals_count: вернуть количество животных на ферме,
get_milk_count: вернуть сколько молока можно получить в день,
get_eggs_count: вернуть сколько яиц можно получить в день.
"""
from topic_07_oop.hw.class_3_1_chicken import Chicken
from topic_07_oop.hw.class_3_2_goat import Goat


class Farm:
    def __init__(self, n, o):
        self.zoo_animals = []
        self.name = n
        self.owner = o

    def get_goat_count(self):
        return len([x for x in self.zoo_animals if isinstance(x, Goat)])

    def get_chicken_count(self):
        return len([y for y in self.zoo_animals if isinstance(y, Chicken)])

    def get_animals_count(self):
        return len(self.zoo_animals)

    def get_milk_count(self):
        return sum([x.milk_per_day for x in self.zoo_animals if isinstance(x, Goat)])

    def get_eggs_count(self):
        return sum([y.eggs_per_day for y in self.zoo_animals if isinstance(y, Chicken)])


if __name__ == '__main__':
    f1 = Farm('Farmerland', 'Jane')
    f1.zoo_animals.append(Chicken('Koko', 4, 18))
    print(f1.get_chicken_count())

    f1.zoo_animals.extend([Goat('Kozlik', 2, 4),
                           Goat('Meme', 3, 6),
                           Chicken('Ryaba', 23, 10),
                           Chicken('Kura', 17, 9)])
    print(f1.get_goat_count())
    print(f1.get_chicken_count())
    print(f1.get_milk_count())
    print(f1.get_eggs_count())