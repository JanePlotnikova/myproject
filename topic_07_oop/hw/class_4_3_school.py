"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""
from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker


class School:
    def __init__(self, people: list, number: int):
        self.people = people
        self.number = number

    def get_avg_mark(self):
        all_m = [m.get_avg_mark() for m in self.people if isinstance(m, Pupil)]
        return sum(all_m)/len(all_m)

    def get_avg_salary(self):
        all_s = [s.salary for s in self.people if isinstance(s, Worker)]
        return sum(all_s) / len(all_s)

    def get_worker_count(self):
        return len([w for w in self.people if isinstance(w, Worker)])

    def get_pupil_count(self):
        return len([p for p in self.people if isinstance(p, Pupil)])

    def get_pupil_names(self):
        return [n.name for n in self.people if isinstance(n, Pupil)]

    def get_unique_worker_positions(self):
        return set([p.position for p in self.people if isinstance(p, Worker)])

    def get_max_pupil_age(self):
        return max([a.age for a in self.people if isinstance(a, Pupil)])

    def get_min_worker_salary(self):
        return min([s.salary for s in self.people if isinstance(s, Worker)])

    def get_min_salary_worker_names(self):
        return [n.name for n in self.people if isinstance(n, Worker) if n.salary == self.get_min_worker_salary()]


if __name__ == '__main__':
    p1 = Pupil('Leo', 15, {'maths': [4, 5, 4], 'english': [3, 5], 'technology': [5, 5, 3]})
    p2 = Pupil('Kris', 14, {'maths': [3, 3, 4], 'english': [3, 5, 5], 'arts': [5, 5]})
    w1 = Worker('Ben', 40000, 'Manager')
    w2 = Worker('John', 70000, 'Regional director')

    school1 = School([p1, p2, w1, w2], 1253)

    print(School.get_avg_mark(school1))
    print(School.get_avg_salary(school1))
    print(School.get_worker_count(school1))
    print(School.get_pupil_count(school1))
    print(School.get_pupil_names(school1))
    print(School.get_unique_worker_positions(school1))
    print(School.get_max_pupil_age(school1))
    print(School.get_min_worker_salary(school1))
    print(School.get_min_salary_worker_names(school1))
