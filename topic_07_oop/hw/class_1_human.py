class Human:
    """
    Класс Human.

    Поля:
    age,
    first_name,
    last_name.

    При создании экземпляра инициализировать поля класса.

    Создать метод get_age, который возвращает возраст человека.

    Перегрузить оператор __eq__, который сравнивает объект человека с другим по атрибутам.

    Перегрузить оператор __str__, который возвращает строку в виде "Имя: first_name last_name Возраст: age".
    """

    def __init__(self, a, f_n, l_n):
        self.age = a
        self.first_name = f_n
        self.last_name = l_n

    def get_age(self):
        return self.age

    def __eq__(self, other):
        return self.age == other.age and self.first_name == other.first_name and self.last_name == other.last_name

    def __str__(self):
        return f'Имя: {self.first_name} {self.last_name} Возраст: {self.age}'

if __name__ == "__main__":
    p1 = Human(20, 'Peter', 'Black')
    p2 = Human(30, 'Ben', 'White')
    print(p1)
    print(p2.get_age())
    print(p1 == p2)
