"""
Класс Pupil.

Поля:
имя: name,
возраст: age,
dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

Методы:
get_all_marks: получить список всех оценок,
get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
get_avg_mark: получить средний балл (все предметы),
__le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
"""

class Pupil:
    def __init__(self, name: str, age: int, marks: dict):
        self.name = name
        self.age = age
        self.marks = marks

    def get_all_marks(self):
        m_list = []
        for m in self.marks.values():
            m_list.extend(m)
        return m_list

    def get_avg_mark_by_subject(self, subj):
        if subj in self.marks.keys():
            return sum(self.marks[subj])/len(self.marks[subj])
        else:
            return 0

    def get_avg_mark(self):
        return sum(self.get_all_marks())/len(self.get_all_marks())

    def __le__(self, other):
        return self.get_avg_mark() <= other.get_avg_mark()


if __name__ == "__main__":
    p1 = Pupil('Leo', 15, {'maths': [4, 5, 4], 'english': [3, 5], 'technology': [5, 5, 3]})
    p2 = Pupil('Kris', 14, {'maths': [3, 3, 4], 'english': [3, 5, 5], 'arts': [5, 5]})
    print(p1.get_all_marks())
    print(p1.get_avg_mark())
    print(p1.get_avg_mark_by_subject('maths'))
    if p1 <= p2:
        print(f'{p1.name} studies worse than {p2.name}')
    else:
        print (f'{p1.name} studies better than {p2.name}')