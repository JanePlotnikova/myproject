"""
Класс Worker.

Поля:
имя: name,
зарплата: salary,
должность: position.

Методы:
__gt__: возвращает результат сравнения (>) зарплат работников.
__len__: возвращает количетсво букв в названии должности.
"""


class Worker:
    def __init__(self, name: str, salary: int, position: str):
        self.name = name
        self.salary = salary
        self.position = position

    def __gt__(self, other):
        return self.salary > other.salary

    def __len__(self):
        return len(self.position)


if __name__ == "__main__":
    w1 = Worker('Ben', 40000, 'Manager')
    w2 = Worker('John', 70000, 'Regional director')
    print(w1 > w2)
    if w1 > w2:
        print(f'{w1.name} earns more than {w2.name}')
    else:
        print(f'{w1.name} earns less than {w2.name}')
    print(len(w1))
